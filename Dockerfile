FROM golang:1.21.0-alpine3.17 as builder
WORKDIR /go/src/github.com/leominov/prometheus-actions
COPY . .
RUN go build -o prometheus-actions ./

FROM alpine:3.17
COPY --from=builder /go/src/github.com/leominov/prometheus-actions/prometheus-actions /usr/local/bin/prometheus-actions
